package main

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"bufio"
)

func checkFile(fileName, expression string) {

	file, err := os.Open(fileName)
	if err != nil {
		panic(err)
	}

	defer file.Close()

	var re = regexp.MustCompile(expression)

	scanner := bufio.NewScanner(file)

	var row int = 1
	var shownTitle bool = false

	for scanner.Scan() {
		var lista []string
		lista = re.FindAllString(scanner.Text(), -1)

		if (len(lista) != 0) {

			if (!shownTitle) {
				fmt.Printf("File: %s\n", fileName)
				shownTitle = true
			}
			fmt.Printf("\trow: %d,\t%s\n", row, lista[0])
		}

		row++
	}
}

func fileIsGroovyFile(fileName string) bool {

	match, _ := regexp.MatchString(".groovy", fileName)

	return match
}

func visit(path string, f os.FileInfo, err error) error {

	if fileIsGroovyFile(path) {
		checkFile(path, "([a-z]{1}\\w+.save\\s*\\()")
	}

	return nil
}

func main() {

	filepath.Walk(".", visit)
}
