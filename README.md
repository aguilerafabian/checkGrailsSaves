# checkGrailsSaves
Golang utility to check some stuff over Grails .save() function

run the script with

```
$ cd <YOUR_GRAILS_PROJECT_FOLDER>
$ go run checkGrailsSaves.go
```

It will print a report in wich every save() is listed.

just to let you know wich file has that property.

report example
```
$ go run checkGrailsSaves.go
File: ApplicantsService.groovy
    row: 90,    applicant.save(
    row: 136,   applicant.save(
$
```

